## My RPM spec files

```
$ sudo dnf in snr wget

rosa2021.1: https://abf.io/platforms/rosa2021.1/products/284
download: rootfs-std-rosa2021.1_x86_64_<date>.tar.xz
$ sudo mkdir /var/lib/machines/rosa2021.1
$ sudo tar -C cd /var/lib/machines/rosa2021.1/ -xpf rootfs-std-rosa2021.1_x86_64_<date>.tar.xz

shell:
$ sudo systemd-nspawn -D /var/lib/machines/rosa2021.1
or:
snr rosa2021.1 --bind=$(PWD):/root/rpmbuild
```
