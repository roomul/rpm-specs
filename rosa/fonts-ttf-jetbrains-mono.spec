Name:           fonts-ttf-jetbrains-mono
Version:        2.304
Release:        1%{?dist}
Summary:        fonts-ttf-jetbrains-mono

License:        OFL1.1
URL:            https://www.jetbrains.com/lp/mono
Source0:        https://download.jetbrains.com/fonts/JetBrainsMono-%{version}.zip

Group:          System/Fonts/True type

BuildRequires:  unzip mkfontdir mkfontscale
Requires:       freetype

BuildArch:      noarch
BuildRoot:      %{_tmppath}/build-root-%{name}
Prefix:         /usr/share/fonts

%description
A typeface for developers

%prep
%setup -q -c -n jetbrains-mono

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{prefix}/jetbrains-mono
find . -type f -name '*.ttf' -exec install -m644 -t $RPM_BUILD_ROOT/%{prefix}/jetbrains-mono/ {} \;

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,0755)
%{prefix}/jetbrains-mono


%changelog
* Wed May 24 2023 AleksTJ
- Initial package
