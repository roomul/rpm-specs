Name:           tio
Version:        2.5
Release:        1%{?dist}
Summary:        A simple serial device I/O tool

Group:          Terminals

License:        GPLv2+
URL:            https://github.com/tio/tio
Source0:        https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.xz

BuildRequires:  meson cmake lib64inih-devel lib64inih0
Requires:       lib64inih0

%description
tio is a simple serial device tool which features a straightforward
command-line and configuration file interface to easily connect to 
serial TTY devices for basic I/O operations.

%prep
%autosetup


%build
%meson
%meson_build


%install
%meson_install


%files
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.*


%changelog
* Wed May 24 2023 AleksTJ
- Initial package
