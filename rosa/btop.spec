%define debug_package %{nil}

Summary:      A system monitor
Name:         btop
Version:      1.2.13
Release:      1
License:      Apache
Group:        Monitoring
Url:          https://github.com/aristocratos/btop
Source0:      https://github.com/aristocratos/btop/archive/refs/tags/v%{version}.tar.gz

%description
Resource monitor that shows usage and stats for processor,
memory, disks, network and processes.

%files
%attr(-, root, root)
%license LICENSE
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/%{name}
%{_bindir}/%{name}

#------------------------------------------------------------------

%prep
%autosetup -p1

%build
%make all

%install
%makeinstall_std PREFIX=%{_prefix}

%clean
rm -rf $RPM_BUILD_DIR
