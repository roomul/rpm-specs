Name:           neutral-cursor-theme
Version:        1.13a
Release:        1%{?dist}
Summary:        Neutral cursor theme

License:        GPLv2+
URL:            https://store.kde.org/p/999947
Source:         neutral-%{version}.tar.gz

Group:          Graphical desktop/GNOME        

BuildArch:      noarch

BuildRequires:  tar


%description
The KDE Neutral cursor theme

%prep
%setup -q -n neutral-%{version}

%build


%install
%{__mkdir_p} -p $RPM_BUILD_ROOT/usr/share/icons/neutral
%{__cp} -r ./* $RPM_BUILD_ROOT/usr/share/icons/neutral/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_datadir}/icons/neutral/
%license LICENSE


%changelog
* Fri May 26 2023 user
- Initial package
