%define	name   snapper-gui
%define debug_package %{nil}

Name:           %{name}
Version:        0.2
Release:        1%{?dist}
Summary:        GUI for snapper

Group:          Archiving/Backup

License:        GPLv2
URL:            https://github.com/ricardomv/snapper-gui
Source:         %{name}-%{version}.tar.gz

BuildRequires:  dbus python3-dbus python-dbus-devel python3-setuptools lib64gtksourceview-3.0-devel lib64gtksourceview-3.0_1
Requires:       dbus snapper python3 gtk3 gtksourceview3 python3-dbus lib64gtksourceview-gir3.0 lib64gtksourceview-3.0_1

%description
A tool for Linux filesystem snapshot management, works with btrfs,
ext4 and thin-provisioned LVM volumes.

%prep
%autosetup -p1

%build
%py3_build

%install
%py3_install

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{python3_sitelib}/*


%changelog
* Sat May 27 2023 root
- Initial package
