%define name arc-icon-theme

Name:           %{name}
Version:        20161122
Release:        1%{?dist}
Summary:        Arc Icon Theme set

Group:          Graphical desktop/Other

License:        GPLv3+
URL:            https://github.com/horst3180/%{name}
Source0:        https://github.com/horst3180/%{name}/archive/refs/tags/20161122.tar.gz      

BuildArch:      noarch

%description
This package contains Arc icon theme

%prep
%autosetup

%build
./autogen.sh --prefix=/usr

%install
%make_install


%files
%{_datadir}/icons/Arc/*

%post
if [ -f "/usr/bin/gtk-update-icon-cache" ]; then
  /usr/bin/gtk-update-icon-cache /usr/share/icons/Arc
fi


%changelog
* Sun May 28 2023 root
- Initial package
